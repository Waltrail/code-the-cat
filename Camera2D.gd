extends Camera2D
var isDragging = false
var OffSet = Vector2()
var isMoving = true
var isZoomAval = true
onready var timer = get_node("../Timer")
onready var level = get_node("../Level")
onready var Code = get_node("../HUD/Panel")
onready var Help = get_node("../HUD/HelpPanel")
func _ready():
	pass # Replace with function body.

func _input(event):
	if !isZoomAval:
		return
	var zoom = get_zoom()
	if (event is InputEventMouseButton):
		if (event.button_index == BUTTON_WHEEL_UP):
			zoom[0] = zoom[0] + 0.1
			zoom[1] = zoom[1] + 0.1
		if (event.button_index == BUTTON_WHEEL_DOWN):
			zoom[0] = zoom[0] - 0.1
			zoom[1] = zoom[1] - 0.1
	if zoom[0] <= 0.2:
		zoom[0] = 0.2
	if zoom[1] <= 0.2:
		zoom[1] = 0.2
		
	if zoom[0] >= 2:
		zoom[0] = 2
	if zoom[1] >= 2:
		zoom[1] = 2
	set_zoom(zoom)

func _process(delta):
	if Input.is_action_just_released("mouseClick"):
		isDragging = false
		OffSet = Vector2()
		timer.start()
	if Code.rect_scale.y <= 0 || Help.rect_scale.y <= 0:
		if !(get_viewport().get_mouse_position().y <= 42 && get_viewport().get_mouse_position().x > 515 && get_viewport().get_mouse_position().x < 765):
			if Input.is_action_just_pressed("mouseClick") && !isDragging:
				smoothing_speed = 30
				isDragging = true
				OffSet = get_global_mouse_position()
				isMoving = false
		if isDragging:
			position -= get_global_mouse_position() - OffSet
			if position.x > limit_right:
				position.x = limit_right - 10
			elif position.x < limit_left:
				position.x = limit_left + 10
			if position.y < limit_top:
				position.y = limit_top - 10
			elif position.y > limit_bottom:
				position.y = limit_bottom + 10
		else:
			if timer.time_left <= 0:
				var cat = level.get_cat()
				if cat:
					smoothing_speed = 4
					isMoving = true
					position = cat * 2
	
