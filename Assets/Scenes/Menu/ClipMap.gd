extends ViewportContainer


onready var Level = $Container/Level
onready var Back = $Container/background
onready var But = $Button
var map = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _setmap():
	Level.SetMapAndCode(map["Map"], [])
	Level.generate_new(map["Map"])
	But.text = map["Name"]
	var size = Level.size
	Level.scale = size / 140
	Back.scale = size / 140

func SetMap(Map):
	map = Map
	call_deferred("_setmap")


func _on_Button_pressed():
	Globals.goto_scene("res://Game.tscn", map)
