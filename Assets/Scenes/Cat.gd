extends Node2D

onready var animationPlayer = $AnimationPlayer
onready var Grid = get_parent()
enum Type{ Empty = -1, Cat, Wall, Flag, Rock, Box, Border }
export var move_speed = 256
export var push_speed = 150
export var tile_size = Vector2(36,18)
export(Type) var type = Type.Cat
var last_position = Vector2()
var target_position = Vector2()
var move_dir = Vector2()
var isPushed = false

func _ready():
	position = position.snapped(tile_size)
	last_position = position
	target_position = position

func _process(delta):
	if $Pivot.position == Vector2.ZERO && !animationPlayer.is_playing():
		animationPlayer.play("Idle")


func update_look_direction(direction):
	if direction.x < 0:
		$Pivot/Cat.flip_h = true
	if direction.x > 0:
		$Pivot/Cat.flip_h = false

func move_to(move_direction):
	update_look_direction(move_direction)
	animationPlayer.play("Run")
	target_position = Vector2(position.x + move_direction.x * tile_size.x, position.y + move_direction.y * tile_size.y)
	$Tween.interpolate_property($Pivot, "position", Vector2(), move_direction * 36, 
		animationPlayer.current_animation_length, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()
	return animationPlayer

func Scan():
	animationPlayer.play("E")
	return animationPlayer
	
func Win():
	animationPlayer.play("Win")
	return animationPlayer
func Lose():
	animationPlayer.play("Lose")
	return animationPlayer
func GetAnim():
	return animationPlayer
