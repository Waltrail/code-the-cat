extends TileMap
export var Width = 1000
export var Height = 720
onready var Anim = get_node("../HUD/AnimationPlayer2")
onready var timer = get_node("../Timer")
onready var CatObj = preload("res://Assets/Scenes/CatObj.tscn")
onready var WallObj = preload("res://Assets/Scenes/Wall.tscn")
onready var FlagObj = preload("res://Assets/Scenes/Flag.tscn")
onready var BorderObj = preload("res://Assets/Scenes/Border.tscn")
onready var BoxObj = preload("res://Assets/Scenes/Box.tscn")
onready var RockObj = preload("res://Assets/Scenes/Rock.tscn")
onready var EmptyObj = preload("res://Assets/Scenes/Empty.tscn")
onready var CodeEditor = get_node("../HUD/Panel/VBoxContainer/Code")
onready var HelpCode = get_node("../HUD/Panel/VBoxContainer/Const")
onready var Cam = get_node("../Camera2D")
signal processed()


enum { Empty = -1, Cat, Wall, Flag, Rock, Box, Border }
enum { Code, Stop, Push, Win }

var AnimCat = null
var error = false
var IsBlocked = false
var size = 0;

var MoveTo = Vector2()

var moveObj = Cat
var stopObj = Wall
var winObj = Flag
var pushObj = Empty

var CurrentLine = -1
var LastLine = -1
var isCodeProcessed = false
var IsWin = false

var Available = [Cat, Wall, Flag]
var DisStop = true
var DisWin  = true
var DisCode = false
var DisPush = true

var Map = [
	"wwwwwwwwwwww",
	"wc_________w",
	"wc_________w",
	"wwwwwwwwwwww",
	"w__________w",
	"w_w___f____w",
	"w__________w",
	"wwwwwwwwwwww",
]
var codeConst = [
	"Disable(Stop)",
	"Disable(Win)",
	"Disable(Push)",
	"Enable(Code)",
	"Add(Cat)",
	"Add(Wall)",
	"Ban(Flag)",
]

func arr_join(arr, separator = ""):
	var output = "";
	for s in arr:
		output += str(s) + separator    
	return output

func SetAll(map, code):
	Map = map
	codeConst = code
	generate_new(Map)

func SetMapAndCode(map, code):
	call_deferred("SetAll", map, code)

func wait():
	var p = get_parent()
	var t = p.get_child(1)
	t.start(1000)
	yield(t,"timeout")

func get_cat():
	var positions = Vector2()
	var cats = 0
	for ch in get_children():
		if ch.type == moveObj:
			positions+=ch.position
			cats+=1
	if cats == 0:
		return
	return positions / cats

func generate_new(map):
	RunCode(codeConst, 0, codeConst.size(), true)
	if HelpCode != null:
		HelpCode.text = arr_join(codeConst, "\n")
	CurrentLine = -1
	LastLine = -1
	MoveTo = Vector2()
	IsWin = false
	isCodeProcessed = false
	if CodeEditor != null:
		CodeEditor.readonly = false
	for child in get_children():
		set_cellv(world_to_map(child.position), -1)
		child.free()
	for row in map.size():
		map[row].to_lower()
	size = Vector2(Width / map[0].length(), Height / map.size())
	for row in map.size():
		for col in map[row].length():
			var newObj = false
			match map[row][col]:
				'w': 
					newObj = WallObj.instance()
				'c':
					newObj = CatObj.instance()
				'f':
					newObj = FlagObj.instance()
				'x':
					newObj = BoxObj.instance()
				'b':
					newObj = BorderObj.instance()
				'r':
					newObj = RockObj.instance()
			if newObj:
				newObj.position = Vector2(row * 36 - col * 36, row * 18 + col * 18)
				add_child(newObj)
	if Cam != null:
		Cam.limit_left = -map.size() * 72 * 2.5
		Cam.limit_right = map.size() * 72 * 2.5
		Cam.limit_top = -map[0].length() * 72 * 2.5
		Cam.limit_bottom = map[0].length() * 72 * 2.5
	var background = get_node("../background")
	for row in map.size() * 8:
		for col in map[map.size() - 1].length() * 8:
			set_cellv(Vector2(map.size() * 4 - row, map[map.size() - 1].length() * 4 - col), -1)
			background.set_cellv(Vector2(map.size() * 4 - row, map[map.size() - 1].length() * 4 - col), 1)
	for child in get_children():
		set_cellv(world_to_map(child.position), -1)
		child.move_to(Vector2.ZERO)
		update_pawn_position(child , world_to_map(child .position), Vector2.ZERO)
		set_cellv(world_to_map(child.position), child.type)
	

func _ready():
	for child in get_children():
		set_cellv(world_to_map(child.position), child.type)
	generate_new(Map)

func To_Isometric(Cartesian):
	return Vector2(Cartesian.x - Cartesian.y,Cartesian.x / 2 + Cartesian.y / 2)

func To_Cartesian(Isometric):
	return Vector2((Isometric.x + Isometric.y * 2) / 2, -Isometric.x + (Isometric.x + Isometric.y * 2) / 2)

func get_input_direction():
	var moveVec = To_Isometric(MoveTo)
	MoveTo = Vector2()
	return moveVec

func _process(delta):
	if IsWin:
		IsBlocked = true
		set_process(false)
		for ch in get_children():
			if ch.type == moveObj:
				ch.Win()
		if Anim != null:
			Anim.play("Win")
			yield(Anim, "animation_finished")
		generate_new(Map)
		set_process(true)
		IsBlocked = false
		return
	if isCodeProcessed || error:
		IsBlocked = true
		set_process(false)
		for ch in get_children():
			if ch.type == moveObj:
				ch.Lose()
		if Anim != null:
			Anim.play("Lose")
			yield(Anim, "animation_finished")
		generate_new(Map)
		set_process(true)
		IsBlocked = false
		return
	for child in get_children():
		set_cellv(world_to_map(child.position), child.type)
	var input_direction = get_input_direction()
	if not input_direction || moveObj == Empty:
		return
	if input_direction:
		IsWin = false
		var anim
		for ch in get_children():
			if ch.type == moveObj:
				var target_position = request_move(ch,input_direction)
				if !target_position:
					anim = ch.Scan()
		if anim != null:
			yield(anim,"animation_finished")
			emit_signal("processed")
			return
	set_process(false)
	if input_direction:
		var anim
		for ch in get_children():
			if ch.type == moveObj:
				anim = ch.move_to(input_direction)
				update_pawn_position(ch, world_to_map(ch.position), input_direction)
		yield(anim,"animation_finished")
		for ch in get_children():
			if ch.type == moveObj:
				ch.get_child(1).stop_all()
				ch.get_child(2).position = Vector2()
				ch.position += input_direction * 36
			elif ch.type == pushObj && ch.isPushed:
				ch.get_child(1).stop_all()
				ch.get_child(2).position = Vector2()
				ch.isPushed = false
				ch.position += input_direction * 36
		emit_signal("processed")
	set_process(true)

func get_cell_pawn(coordinates):
	for node in get_children():
		if world_to_map(node.position) == coordinates:
			return(node)

func request_move(pawn, direction):
	var cell_start = world_to_map(pawn.position)
	var cell_target = cell_start + To_Cartesian(direction)
	var cell_target_type = get_cellv(cell_target)
	match cell_target_type:
		Empty:
			print("EMPTY")
			return true
		Cat:
			if winObj == Cat:
				IsWin = true
			if pawn.type == Cat:
				return true
			if pushObj == Cat:
				var pushes = []
				for child in get_children():
					if  world_to_map(child.position) == cell_target:
						pushes.append(child)
						break
				if pushes.size() == 0:
					return false
				var isOkToMove = FindNextPush(pushes, To_Cartesian(direction))
				if isOkToMove:
					for x in pushes:
						x.move_to(direction)
					return true
				return false
			if stopObj != Cat:
				return pawn.type != stopObj
		Wall:
			if winObj == Wall:
				IsWin = true
			if pawn.type == Wall:
				return true
			if pushObj == Wall:
				var pushes = []
				for child in get_children():
					if  world_to_map(child.position) == cell_target:
						pushes.append(child)
						break
				if pushes.size() == 0:
					return false
				var isOkToMove = FindNextPush(pushes, To_Cartesian(direction))
				if isOkToMove:
					for x in pushes:
						x.move_to(direction)
					return true
				return false
			if stopObj != Wall:
				return pawn.type != stopObj
			IsWin = false
		Flag:
			if winObj == Flag:
				IsWin = true
			if pawn.type == Flag:
				return true
			if pushObj == Flag:
				var pushes = []
				for child in get_children():
					if  world_to_map(child.position) == cell_target:
						pushes.append(child)
						break
				if pushes.size() == 0:
					return false
				var isOkToMove = FindNextPush(pushes, To_Cartesian(direction))
				if isOkToMove:
					for x in pushes:
						x.move_to(direction)
					return true
				return false
			if stopObj != Flag:
				return pawn.type != Flag
			IsWin = false
		Box:
			print("BOX")
			print(pushObj)
			if winObj == Box:
				print("WIN")
				IsWin = true
			if pawn.type == Box:
				print("SAME")
				return true
			if pushObj == Box:
				print("PUSH")
				var pushes = []
				for child in get_children():
					if  world_to_map(child.position) == cell_target:
						pushes.append(child)
						break
				if pushes.size() == 0:
					return false
				var isOkToMove = FindNextPush(pushes, To_Cartesian(direction))
				if isOkToMove:
					for x in pushes:
						x.move_to(direction)
					return true
				return false
			if stopObj != Box:
				print("STOP")
				print(pawn.type, " ", cell_target_type)
				return pawn.type != Box
			IsWin = false
			print("ELSE")
		Rock:
			if winObj == Rock:
				IsWin = true
			if pawn.type == Rock:
				return true
			if pushObj == Rock:
				var pushes = []
				for child in get_children():
					if  world_to_map(child.position) == cell_target:
						pushes.append(child)
						break
				if pushes.size() == 0:
					return false
				var isOkToMove = FindNextPush(pushes, To_Cartesian(direction))
				if isOkToMove:
					for x in pushes:
						x.move_to(direction)
						x.isPushed = true
					return true
				return false
			if stopObj != Rock:
				return pawn.type != Rock
			IsWin = false
		Border:
			return false

func FindObjectByPosition(Position:Vector2):
	var all = []
	print("By ",Position)
	for child in get_children():
		print("child: ", child.position)
		if child.position == Position:
			all.append(child)
	return all

func FindNextPush(pushObjs:Array, dir):
	var cell_start = world_to_map(pushObjs.back().position)
	var cell_target = cell_start + dir
	var cell_target_objects = FindObjectByPosition(map_to_world(cell_target))
	print(cell_target_objects.size())
	for obj in cell_target_objects:
		if obj.type == stopObj:
			return false
		elif obj.type == pushObj:
			pushObjs.append(obj)
			return FindNextPush(pushObjs, dir)
	if pushObjs.back().type == stopObj && !cell_target_objects:
		return false
	return true

func update_pawn_position(pawn, cell_start, dir):	
	set_cellv(cell_start + To_Cartesian(dir), pawn.type)
	set_cellv(cell_start, Empty)
	return map_to_world(cell_start + To_Cartesian(dir)) + cell_size / 2

var Variables = Dictionary()
func RunCode(Code, lineIndex:int, stop:int, addConst = false) -> GDScriptFunctionState:
	if IsBlocked:
		return
	CurrentLine = lineIndex
	var State = null
	if lineIndex == 0 && !addConst:
		CodeEditor.readonly = true
		if LastLine == -1:
			LastLine = stop
		Variables.clear()
	if lineIndex + 1 > stop && lineIndex + 1 > Code.size():
		isCodeProcessed = true
		return State
	if lineIndex == 0:
		error = false
	if error:
		return State
	var line = Code[lineIndex].replace("\t", "").replace(" ", "")
	if "if" in line || "elseif" in line ||"else" in line:
		State = Branching(Code,lineIndex, stop)
		return State
	elif "for" in line:
		State = Loop(Code,lineIndex, stop)
		return State
	elif ("Code" in line || "code" in line) && "=" in line:
		if !DisCode || addConst:
			var what = line.replace(" ", "").split("=")[1]
			if "Cat" in what || "cat" in what:
				if Available.has(Cat) || addConst:
					moveObj = Cat
			elif "Wall" in what || "wall" in what:
				if Available.has(Wall) || addConst:
					moveObj = Wall
			elif "Flag" in what || "flag" in what:
				if Available.has(Flag) || addConst:
					moveObj = Flag
			elif "Rock" in what || "rock" in what:
				if Available.has(Rock) || addConst:
					moveObj = Rock
			elif "Box" in what || "box" in what:
				if Available.has(Box) || addConst:
					moveObj = Box
	elif ("Stop" in line || "stop" in line) && "=" in line:
		if !DisStop || addConst:
			var what = line.replace(" ", "").split("=")[1]
			if "Cat" in what || "cat" in what:
				if Available.has(Cat) || addConst:
					stopObj = Cat
			elif "Wall" in what || "wall" in what:
				if Available.has(Wall) || addConst:
					stopObj = Wall
			elif "Flag" in what || "flag" in what:
				if Available.has(Flag) || addConst:
					stopObj = Flag
			elif "Rock" in what || "rock" in what:
				if Available.has(Rock) || addConst:
					stopObj = Rock
			elif "Box" in what || "box" in what:
				if Available.has(Box) || addConst:
					stopObj = Box
	elif ("Push" in line || "push" in line) && "=" in line:
		if !DisPush || addConst:
			var what = line.replace(" ", "").split("=")[1]
			if "Cat" in what || "cat" in what:
				if Available.has(Cat) || addConst:
					pushObj = Cat
			elif "Wall" in what || "wall" in what:
				if Available.has(Wall) || addConst:
					pushObj = Wall
			elif "Flag" in what || "flag" in what:
				if Available.has(Flag) || addConst:
					pushObj = Flag
			elif "Rock" in what || "rock" in what:
				if Available.has(Rock) || addConst:
					pushObj = Rock
			elif "Box" in what || "box" in what:
				if Available.has(Box) || addConst:
					pushObj = Box
	elif ("Win" in line || "win" in line) && "=" in line:
		if !DisWin || addConst:
			var what = line.replace(" ", "").split("=")[1]
			if "Cat" in what || "cat" in what:
				if Available.has(Cat) || addConst:
					winObj = Cat
			elif "Wall" in what || "wall" in what:
				if Available.has(Wall) || addConst:
					winObj = Wall
			elif "Flag" in what || "flag" in what:
				if Available.has(Flag) || addConst:
					winObj = Flag
			elif "Rock" in what || "rock" in what:
				if Available.has(Rock) || addConst:
					winObj = Rock
			elif "Box" in what || "box" in what:
				if Available.has(Box) || addConst:
					winObj = Box
	elif "Move" in line || "move" in line:
		if moveObj != Empty:
			var where = line.split("(")[1].replace(")", "").replace(" ", "")
			var dir = Vector2()
			if "Down" in where or "down" in where:
				dir = Vector2(0,1)
			elif "Up" in where or "up" in where:
				dir = Vector2(0, -1)
			elif "Right" in where or "right" in where:
				dir = Vector2(1,0)
			elif "Left" in where or "left" in where:
				dir = Vector2(-1,0)
			if dir != Vector2.ZERO:
				MoveTo = dir
				State = yield(self,"processed")
	elif "print" in line:
		print(Calc(line.substr(line.find("(") + 1, line.find_last(")")- line.find("(")-1)))
	elif "+=" in line:
		var name = line.split("+")[0].trim_suffix(" ").trim_prefix(" ")
		var ex = Code[lineIndex].replace(" ", "")
		if Variables.has(name):
			Variables[name] = Calc(ex)
	elif "*=" in line:
		var name = line.split("*")[0].trim_suffix(" ").trim_prefix(" ")
		var ex = Code[lineIndex].replace(" ", "")
		if Variables.has(name):
			Variables[name] = Calc(ex)
	elif "-=" in line:
		var name = line.split("-")[0].trim_suffix(" ").trim_prefix(" ")
		var ex = Code[lineIndex].replace(" ", "")
		if Variables.has(name):
			Variables[name] = Calc(ex)
	elif "/=" in line:
		var name = line.split("/")[0].trim_suffix(" ").trim_prefix(" ")
		var ex = Code[lineIndex].replace(" ", "")
		if Variables.has(name):
			Variables[name] = Calc(ex)	
	elif "++" in line:
		var name = line.split("++")[0].trim_suffix(" ").trim_prefix(" ")
		if Variables.has(name):
			Variables[name] += 1
	elif "--" in line:
		var name = line.split("--")[0].trim_suffix(" ").trim_prefix(" ")
		if Variables.has(name):
			Variables[name] -= 1
	elif "=" in line:
		Assign(Code[lineIndex])	
	if lineIndex + 1 < stop && lineIndex + 1 < Code.size():
		return RunCode(Code, lineIndex + 1, stop, addConst)
	else:
		if stop == LastLine:
			isCodeProcessed = true
		
		return State

func Assign(Code:String):
	var Name = Code.split('=')[0].trim_suffix(" ").trim_prefix(" ")
	var StrValue = Code.split('=')[1]
	if "*" in StrValue || "/" in StrValue || "+" in StrValue || "-" in StrValue:
		StrValue = Calc(StrValue)
	Variables[Name] = float(StrValue)

func Convert(value:String):
	var name = value.trim_prefix(" ").trim_suffix(" ").replace("+", "").replace("-", "")
	if Variables.has(name):
		if "-" in value:
			return -Variables[name]
		return Variables[name]
	if "\"" in value:
		return value.replace("\"", "")
	return float(value)

func FindClosest(Ex:String, finds, index, right = true):
	var closest = -1
	if right:
		for i in range(index - 1):
			for f in finds.size():
				if finds[f] == Ex[i] && i > closest:
					closest = i
		return closest
	else:
		closest = 100000000000
		for i in range(index + 2, Ex.length()):
			for f in finds.size():
				if finds[f] == Ex[i] && i < closest:
					closest = i
		if closest == 100000000000:
			closest = -1
		return closest

func Calc(Ex:String):
	Ex = Ex.replace(" ", "").replace("=", "")
	if "*" in Ex || "/" in Ex:
		var IndexM = Ex.find("*")
		var IndexD = Ex.find("/")
		if IndexM <= 0 || IndexD < IndexM && IndexD > 0:
			var prev = FindClosest(Ex, ["-", "+", "*"], IndexD) + 1
			var next = FindClosest(Ex, ["-", "+", "*", "/"], IndexD, false) - 1
			if prev < 0:
				prev = 0
			if next < 0:
				next = Ex.length()			
			var num1 = Convert(Ex.substr(prev, IndexD - prev))
			var num2 = Convert(Ex.substr(IndexD + 1, next - IndexD))
			if num2 == 0:
				error = true
				# Деление на 0
				return 0
			var result = num1 / num2
			Ex.erase(prev, next - prev + 1)
			Ex = Ex.insert(prev, str(result))
			if "/" in Ex || "*" in Ex || "+" in Ex || "-" in Ex:
				return Calc(Ex)
			return result
		elif IndexD <= 0 || IndexM < IndexD && IndexM > 0: # 4
			var prev = FindClosest(Ex, ["-", "+", "/"], IndexM) + 1 # 2
			var next = FindClosest(Ex, ["-", "+", "*", "/"], IndexM, false) - 1 # 6
			if prev < 0:
				prev = 0
			if next < 0:
				next = Ex.length()			
			var num1 = Convert(Ex.substr(prev, IndexM - prev))
			var num2 = Convert(Ex.substr(IndexM + 1, next - IndexM))
			var result = num1 * num2
			Ex.erase(prev, next - prev + 1)
			Ex = Ex.insert(prev, str(result))
			if "/" in Ex || "*" in Ex || "+" in Ex || "-" in Ex:
				return Calc(Ex)
			return result
	if "-" in Ex.substr(1) || "+" in Ex.substr(1):
		var IndexP = Ex.find("+", 1)
		var IndexS = Ex.find("-", 1)
		var Index = 0
		if IndexP < 0:
			Index = IndexS
		elif IndexS < 0:
			Index = IndexP
		elif IndexP < IndexS:
			Index = IndexP
		else:
			Index = IndexS
		var prev = 0
		var next = FindClosest(Ex, ["+", "-"], Index, false)
		if next < 0:
			next = Ex.length()		
		var num1 = Convert(Ex.substr(prev, Index))
		var num2 = Convert(Ex.substr(Index, next - Index))
		var result = num1 + num2
		Ex.erase(prev, next)
		Ex = Ex.insert(prev, str(result))
		if "+" in Ex || "-" in Ex:
			return Calc(Ex)
		return result
	return Convert(Ex)

func FindBranch(Code, start, stop):
	var EndBlock = start
	var EndBranch = start
	var isBlock = false
	var Opens = 0
	var Closes = 0
	for line in range(start, stop):
		if "{" in Code[line]:
			Opens += 1
		if "}" in Code[line]:
			Closes += 1
		if Opens != 0 && Opens == Closes:
			if !isBlock:
				isBlock = true
				EndBlock = line
			elif !"else" in Code[line] && !"" in Code[line]:
				EndBranch = line 
	if EndBranch == start:
		EndBranch = EndBlock
	if EndBranch < EndBlock:
		EndBranch = stop
	return [EndBlock, EndBranch]

func Branching(Code, lineIndex:int, stop:int) -> GDScriptFunctionState:
	var Branch = FindBranch(Code, lineIndex, stop)
	var BlockEnd = Branch[0]
	var ChainEnd = Branch[1]
	var b = Code[lineIndex].substr(Code[lineIndex].find("(")+1, Code[lineIndex].find(")") - Code[lineIndex].find("(") - 1)
	if Cond(b):
		var state = null
		var f1 = RunCode(Code, lineIndex + 1, BlockEnd)
		if f1 != null:
			state = yield(f1, "completed")
		var f2 = RunCode(Code, ChainEnd, stop)
		if f2 != null:
			state = yield(f2, "completed")
		return state
	else:
		return yield(RunCode(Code, BlockEnd + 1, stop), "completed")

func Loop(Code, lineIndex:int, stop:int) -> GDScriptFunctionState:
	var State = null
	var inside = Code[lineIndex].substr(Code[lineIndex].find("(")+1, Code[lineIndex].find(")") - Code[lineIndex].find("(") - 1)
	inside = inside.replace(" ", "").split("to") 
	var num = inside[0].split("=")
	var numTo = Calc(inside[1])
	var name = num[0]
	num = Calc(num[1])
	Variables[name] = num
	var Start = lineIndex+1
	var EndBlock = lineIndex
	var Opens = 0
	var Closes = 0
	for line in range(lineIndex, stop):
		if "{" in Code[line]:
			Opens += 1
		if "}" in Code[line]:
			Closes += 1
		if Opens != 0 && Opens == Closes:
			EndBlock = line
			break
	var enumer
	if numTo > Variables[name]:
		enumer = 1
	else:
		enumer = -1
	for _x in range(num, numTo):
		State = RunCode(Code, Start, EndBlock)
		if State != null:
			yield(State, "completed")
		if Variables.has(name):
			Variables[name] += enumer
	Variables.erase(name)
	State = RunCode(Code, EndBlock, stop)
	return State

func BoolEq(Ex:String):
	if "<" in Ex:
		return LessThan(Ex)
	elif ">" in Ex:
		return MoreThan(Ex)
	elif "<=" in Ex:
		return LessEqThan(Ex)
	elif ">=" in Ex:
		return MoreEqThan(Ex)
	elif "==" in Ex:
		return Equals(Ex)
	elif "!=" in Ex:
		return !Equals(Ex)

func LessThan(Code:String):
	var con = Code.split("<")
	con[0] = con[0].trim_suffix(" ").trim_prefix(" ")
	con[1] = con[1].trim_suffix(" ").trim_prefix(" ")
	var num1 = Calc(con[0])
	var num2 = Calc(con[1])
	return num1 < num2

func MoreThan(Code:String):
	var con = Code.split(">")
	con[0] = con[0].trim_suffix(" ").trim_prefix(" ")
	con[1] = con[1].trim_suffix(" ").trim_prefix(" ")
	var num1 = Calc(con[0])
	var num2 = Calc(con[1])
	return num1 > num2
	
func LessEqThan(Code:String):
	var con = Code.split("<=")
	con[0] = con[0].trim_suffix(" ").trim_prefix(" ")
	con[1] = con[1].trim_suffix(" ").trim_prefix(" ")
	var num1 = Calc(con[0])
	var num2 = Calc(con[1])
	return num1 <= num2

func MoreEqThan(Code:String):
	var con = Code.split(">=")
	con[0] = con[0].trim_suffix(" ").trim_prefix(" ")
	con[1] = con[1].trim_suffix(" ").trim_prefix(" ")
	var num1 = Calc(con[0])
	var num2 = Calc(con[1])
	return num1 >= num2

func Equals(Code:String):
	Code = Code.replace("!", "=")
	var con = Code.split("==")
	con[0] = con[0].trim_suffix(" ").trim_prefix(" ")
	con[1] = con[1].trim_suffix(" ").trim_prefix(" ")
	var num1 = Calc(con[0])
	var num2 = Calc(con[1])
	return num1 == num2

func Cond(Ex:String):
	if "else" in Ex:
		return true
	Ex = Ex.replace(" ", "")
	if "&&" in Ex:
		var Index = Ex.find("&&")		
		var prev = FindClosest(Ex, ["||"], Index) + 1
		var next = FindClosest(Ex, ["&&", "||"], Index, false) - 1
		if prev < 0:
			prev = 0
		if next < 0:
			next = Ex.length()
		var num1 = BoolEq(Ex.substr(prev, Index - prev))
		var num2 = BoolEq(Ex.substr(Index + 1, next - Index))
		var result = num1 && num2
		Ex.erase(prev, next - prev + 1)
		Ex = Ex.insert(prev, str(result))
		if "&&" in Ex || "||" in Ex:
			return Cond(Ex)
		return result
	if "||" in Ex.substr(1):
		var Index = Ex.find("||", 1)
		var prev = 0
		var next = FindClosest(Ex, ["||"], Index, false)
		if next < 0:
			next = Ex.length()
		var num1 = BoolEq(Ex.substr(prev, Index))
		var num2 = BoolEq(Ex.substr(Index, next - Index))
		var result = num1 || num2
		Ex.erase(prev, next)
		Ex = Ex.insert(prev, str(result))
		if "||" in Ex:
			return Cond(Ex)
		return result
	return BoolEq(Ex)
