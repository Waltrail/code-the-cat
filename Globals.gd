extends Node

var current_scene = null
var Maps = []
var UserMaps = []

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	Maps.append({
		"Name": "Template map",
		"Map": [
			"wwwwwwwwwwww",
			"wc_________w",
			"wc_________w",
			"wwwwwwwwwwww",
			"w__________w",
			"w_w___f____w",
			"w__________w",
			"wwwwwwwwwwww"
		],
		"Help": "Возпользуйтесь циклом, ",
		"Code": [
			"Add(Cat)",
			"Add(Wall)",
			"Add(Flag)",
			"Ban(Cat)",
			"Ban(Wall)",
			"Ban(Flag)",
			"Enable(stop)",
			"Enable(win)",
			"Enable(push)",
			"Disable(stop)",
			"Disable(win)",
			"Disable(code)",
			"Disable(push)",
			"Enable(code)",
			"Code=Cat",
			"Stop=Cat",
			"Win=Flag"
		]
	})
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = load("res://Sounds/loop.mp3")
	player.volume_db = 0.5
	player.play()

func goto_scene(path, Map = null):
	call_deferred("_deferred_goto_scene", path, Map)


func _deferred_goto_scene(path, Map = null):    
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	if Map != null && current_scene.has_method("SetMapAndCode"):
		current_scene.SetMapAndCode(Map["Map"], Map["Code"], Map["Help"])
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)
